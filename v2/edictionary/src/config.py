import os

USE_COLORS = True

# HOSTNAME = "www.collinsdictionary.com"
# HTTP_PATH = "/dictionary/english/"
# SYN_HTTP_PATH = "/dictionary/english-thesaurus/"

PROJECT_PATH = os.path.dirname(os.getcwd())
# HTML_DIR_PATH = os.path.join(PROJECT_PATH, "html_cache")
# HTML_SOURCE_PATH = os.path.join(PROJECT_PATH, "html_permanent", "html")
JSON_DIR_PATH = os.path.join(PROJECT_PATH, "json_cache")

