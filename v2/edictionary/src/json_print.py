from src.json_reader import JsonReader
from src.json_learn_reader import JsonLearnReader
from src.json_syn_reader import JsonSynReader


class JsonPrinter:
    def output(self, text):
        print(text)

    def print(self, content):
        reader = JsonReader(content)
        text = reader.read_content()
        self.output(text)

    def print_learn(self, content):
        reader = JsonLearnReader(content)
        text = reader.read_content()
        self.output(text)

    def print_syn(self, content):
        reader = JsonSynReader(content)
        text = reader.read_content()
        self.output(text)
